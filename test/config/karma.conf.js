module.exports = function (config) {
	'use strict';

	config.set({
		basePath: '../../',
		frameworks: ['jasmine'],
		files: [
			'node_modules/angular/angular.js',
			'node_modules/angular-route/angular-route.js',
			'node_modules/angular-resource/angular-resource.js',
			'node_modules/angular-mocks/angular-mocks.js',
			'js/**/*.js',
			'test/unit/**/*.js'
		],
		autoWatch: false,
		singleRun: true,
		browsers: ['ChromeHeadless'],
		customLaunchers: {
		  ChromeHeadless: {
			base: 'Chrome',
			flags: [
			  '--headless',
			  '--disable-gpu',
			  '--remote-debugging-port=9222',
			  '--no-sandbox',
			  '--disable-dev-shm-usage'
			]
		  }
		}
	});
};
